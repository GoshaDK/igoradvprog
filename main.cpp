#include <iostream>
#include <thread>
#include <mutex>
#include <condition_variable>

using namespace std;

mutex mtx;
condition_variable x;

void read()
{
	unique_lock<mutex> lockRead(mtx);
	x.wait(lockRead);					//no need in unlocking, when wait finished lockRead unlocked.
	cout << "reading" << endl;
}

void write()
{
	unique_lock<mutex> lockWrite(mtx);
	cout << "writing" << endl;
	x.notify_all();
}

void main()
{
	thread read0(read);
	thread read1(read);
	thread read2(read);
	thread read3(read);
	thread write0(write);
	thread write1(write);
	read0.join();
	write0.join();
	read1.join();
	read2.join();
	write1.join();
	read3.join();
	system("pause");
}