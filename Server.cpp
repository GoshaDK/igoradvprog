#include <windows.h> <WINDOWS.H>
#include <strsafe.h> <STRSAFE.H>
#include <stdio.h><STDIO.H>
#include <iostream>
#include <string>

#pragma warning(disable:4996)
#define BUF_SIZE 255
#define NUMOFT 5

void DisplayMessage(HANDLE hScreen,	char *ThreadName, int Data, int Count)
{
	TCHAR msgBuf[BUF_SIZE];
	size_t cchStringSize;
	DWORD dwChars;

	StringCchPrintf(msgBuf, BUF_SIZE,TEXT("Executing iteration %02d of %s having data = %02d \n"), Count, ThreadName, Data);
	StringCchLength(msgBuf, BUF_SIZE, &cchStringSize);
	WriteConsole(hScreen, msgBuf, cchStringSize, &dwChars, NULL);
	Sleep(1000);
}

DWORD WINAPI Thread(LPVOID lpParam)
{
	int     Data = 0;
	int     count = 0;
	HANDLE  hStdout = NULL;
	char Name[20];

	if ((hStdout = GetStdHandle(STD_OUTPUT_HANDLE)) == INVALID_HANDLE_VALUE)
		return 1;
	for (count = 1; count <= NUMOFT; count++)
	{ 
		sprintf(Name, "Thread_no_%d", count);
		Data++;
		DisplayMessage(hStdout, Name, Data, count);
	}

	return 0;
}


void main()
{
	int Data_Of_Thread[NUMOFT];
	HANDLE Handle_Of_Thread[NUMOFT];
	HANDLE Array_Of_Thread_Handles[NUMOFT];

	for (int i = 0; i < NUMOFT; i++)
	{
		Data_Of_Thread[i] = i + 1;
		Handle_Of_Thread[i] = (HANDLE)i;
		Handle_Of_Thread[i] = CreateThread(NULL, 0, Thread, Data_Of_Thread, 0, NULL);
		if (Handle_Of_Thread[i] == NULL)
			ExitProcess(Data_Of_Thread[i]);
		Array_Of_Thread_Handles[i] = Handle_Of_Thread[i];
	}

	WaitForMultipleObjects(1, Array_Of_Thread_Handles, TRUE, INFINITE);

	printf("Since All threads executed lets close their handles \n");

	for (int i = 0; i < NUMOFT; i++)
	{
		CloseHandle(Handle_Of_Thread[i]);
	}
	system("pause");
}